﻿using System;

namespace ninject_auth_example
{
    public class Logger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine($"Log: " + message);
        }

        public void Error(string message)
        {
            Console.WriteLine($"Error: " + message);
        }
    }
}
