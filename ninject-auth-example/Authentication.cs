﻿using System;

namespace ninject_auth_example
{
    public class Authentication
    {
        private readonly IDataSource _dataSource;
        private readonly ILogger _logger;

        public Authentication(IDataSource dataSource, ILogger logger)
        {
            _dataSource = dataSource;
            _logger = logger;
        }

        public void Login(User user)
        {
            try
            {
                _logger.Log("Logging in");

                var model = _dataSource.Save(user);

                _logger.Log("Login successful Id : " + model.Id + " / " + model.Name + " - " + model.Surname + " / " + model.Email + "");
            }
            catch (Exception msg)
            {
                _logger.Error("Error when login process,try again: {0}" + msg);
            }
        }
    }
}
