﻿namespace ninject_auth_example
{
    public interface ILogger
    {
        void Log(string message);
        void Error(string message);
    }
}
