﻿namespace ninject_auth_example
{
    public interface IDataSource
    {
        User Save(User user);
    }
}
