﻿using System;
using Ninject;

namespace ninject_auth_example
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new User
            {
                Id = 125,
                Name = "John",
                Surname = "Doe",
                Email = "johndoe@mail.com"
            };
            
            IKernel kernel = new StandardKernel();

            kernel.Bind<IDataSource>().To<TextDataSource>(); // return textdatasource when idatasource is requested
            kernel.Bind<ILogger>().To<Logger>(); // return logger when ilogger is requested

            var authentication = kernel.Get<Authentication>(); // request authentication instantination from kernel.
            authentication.Login(user);

            Console.ReadKey();
        }
    }
}
